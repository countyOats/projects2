﻿/* Program which performs sort and search algorithms on given .txt files with */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

/* SORT ALGORITHMS
 0 MergeSort
 0 BinarySearchTree
 0 QuickSort
 0 BubbleSort
 */

/* SEARCH ALGORITHMS
 0 BinarySearchTree
 0 Binary Seach
 0 Linear search
*/

namespace SSWeatherData
{
	class Program
	{
		// Main
		static void Main(string[] args)
		{
			//Get data from txt
			Program weatherProgram = new Program();
			SortedDictionary<string, List<double>> weatherData = weatherProgram.GetData();

			// Loops through menu till user chooses to exit program
			while (true)
			{
				int exitVal = MainMenu(weatherData);
				if (exitVal == 1){ break; }
			}
			Console.WriteLine("\nPress any key to exit.");
			Console.ReadKey(true);
		}
		
		// Function that prints out menu options and takes user input
		static int MainMenu(SortedDictionary<string, List<double>> weatherData)
		{
			bool sortFlag; bool searchFlag; // bool to check which loop to run based on user's input
			bool descending; // Wheather to print sorted list in ascending or descending order
			int selectedList; // User selected list
			bool check; // Flag - whether user input is in correct format

			SearchAlgorithms searchAlgos = new SearchAlgorithms();
			SortAlgorithms sortAlgos = new SortAlgorithms();

			// Main for user to choose an List
			while (true)
			{
				int i = 1;

				Console.WriteLine("==========================================");
				Console.WriteLine("||Please select an array to analyse:    ||\n" +
					"==========================================");

				// Prints list of weather arrays to select from
				foreach (KeyValuePair<string, List<double>> list in weatherData)
				{ 
					Console.WriteLine("||{0} - {1}   			||", i, list.Key);
					i++;
				}
				Console.WriteLine("||0 - Exit				||");
				Console.Write("==========================================\n>> ");

				// Checks if user input is in correct format
				check = int.TryParse(Console.ReadLine(), out selectedList);
				// Checks if user input is within range
				if (check == true && selectedList >= 0 && selectedList <= weatherData.Count)
				{
					if (selectedList == 0) { return 1; } // Exit program if user input 0
					break;
				}
				else { Console.WriteLine("\nPlease enter an integer between 0 and {0}.\n", weatherData.Count); }
			}

			// decrements by 1 to get right value from list
			selectedList--;

			// Menu to select action to perform on array
			while (true)
			{
				// Options for menu
				descending = false;
				sortFlag = true;
				searchFlag = false;

				// Prints out user menu with options
				Console.WriteLine("\n==========================================");
				Console.WriteLine("||Please select an action to perform on ||\n" +
					"||the selected array: {0}   	||\n" +
					"==========================================", weatherData.Keys.ElementAt(selectedList));

				// List of options printed out
				printOpts(new string[] { "Sort in ascending order", "Sort in descending order", "Check list for value" });

				check = int.TryParse(Console.ReadLine(), out int selectedOpt); // Checks if user input is in correct format
				if (check == true && selectedOpt >= 0 && selectedOpt < 4) // Checks if user selected a vaild option
				{
					// Implment list indexing option
					if (selectedOpt == 0) { return 1; } // Exit if user input is 0
					if (selectedOpt == 1) { } // sorts list in ascending order
					else if (selectedOpt == 2) // sorts list in descending order
					{
						descending = true;
					}
					else // Search for value
					{
						sortFlag = false; searchFlag = true;
					}
					break;
				}
				else
				{
					Console.WriteLine("\nPlease enter an integer between 0 and 3.\n");
				}
			}

			int increment = 10; // Default increment value (vaild for *_256 lists)
			if (weatherData.Keys.ElementAt(selectedList).Contains("_2048") == true) { increment = 50; }
			else if (weatherData.Keys.ElementAt(selectedList).Contains("_4096") == true) { increment = 80; }

			// Sorting options menu
			// Runs if 1 or 2 selected from previous menu
			while (sortFlag)
			{
				Console.WriteLine("==========================================");
				Console.WriteLine("||Please select a sorting algorithm:	||\n" +
					"==========================================");

				// Prints list of weather arrays to select from
				printOpts( new string[] { "Merge Sort	", "Quick Sort	", "Bubble Sort	", "Binary Tree	" } );

				// Checks if user input is in correct format
				check = int.TryParse(Console.ReadLine(), out int selectedSort); // Checks if user input is in correct format
				if (check == true && selectedSort == 0) { return 1; } // If user inputs 0, exits program
				if (check == true && selectedSort > 0 && selectedSort < 5) // Checks if user selected a vaild option
				{
					// Make fresh copy of list, gets list at key's index
					List<double> editList = new List<double>(weatherData[weatherData.Keys.ElementAt(selectedList)]);

					// Incremental values for when printing out sorted lists
					if (selectedSort == 1) // Merge sort selected
					{
						sortAlgos.MergeSort(editList, (editList.Count / 2) - 1, editList.Count - 1, descending);
					}
					else if (selectedSort == 2) // Quick sort selected
					{
						sortAlgos.QuickSort(editList, 0, editList.Count - 1, descending);
					}
					else if (selectedSort == 3) // Bubble sort selected
					{
						sortAlgos.BubbleSort(editList, descending);
					}
					else // BST selected
					{
						BinarySearchTree bst = new BinarySearchTree();
						bst.CreateBST(editList, true, null, increment, descending); // creates BST, has own printing function
					}

					// Prints out normally sorts apart from bst selected
					if (Enumerable.Range(1, 3).Contains(selectedSort))
					{
						sortAlgos.PrintList(editList, increment); // If not BST, print out values as BST has its own printing function
					}
					break;

				}
				else { Console.WriteLine("\nPlease enter an integer between 1 and 4.\n"); }
			}

			// Searching algorithms menu
			while (searchFlag)
			{
				Console.WriteLine("==========================================");
				Console.WriteLine("||Please select a search algorithm:	||\n" +
					"==========================================");

				// Prints list of weather arrays to select from

				printOpts( new string[] { "Linear Search	", "Binary Search	", "Binary Tree Search" } );

				check = int.TryParse(Console.ReadLine(), out int selectedSch); // Checks if user input is in correct format

				if (check == true && selectedSch == 0) { return 1; } // Exits program if user selects 0
				if (check == true && selectedSch > 0 && selectedSch < 4)
				{
					List<double> editList = new List<double>( weatherData[weatherData.Keys.ElementAt(selectedList)] ); // Makes fresh copy

					double val;

					// Loops till user gives correct input
					while (true)
					{
						Console.Write("Please enter a value to search for: ");
						bool valCheck = double.TryParse(Console.ReadLine(), out val);
						if (valCheck == false) { Console.WriteLine("Please enter a vaild interger or decimal number"); }
						else { break; }
					}

					if (selectedSch == 1) // Linear search selected, doesn't need sorted list
					{
						searchAlgos.LinearSearch(editList, val); // Takes unsorted list and value to find
						searchAlgos.PrintValues(); // Will print out closest values if user value not found
					}
					else if (selectedSch == 2) // Binary search, needs sorted list
					{
						// Selects appropriate sorting algorithm
						if (editList.Count == 256 || editList.Count == 512) // Uses quicksort for 256 and 512
						{
							sortAlgos.BubbleSort(editList, false);
						}
						else if (editList.Count == 2048) // Uses merge sort for 2048
						{
							sortAlgos.MergeSort(editList, (editList.Count / 2) - 1, editList.Count - 1, false);
						}
						else // Uses quick sort for 4096 and 8192
						{
							sortAlgos.QuickSort(editList, 0, editList.Count - 1, false);
						}

						// Returns now ordered edit list
						searchAlgos.BinarySearch(editList, 0, editList.Count/2, editList.Count-1, val);

						// Makes fresh copy list for unsorted searching
						editList = new List<double>(weatherData[weatherData.Keys.ElementAt(selectedList)]);

						int unsortedIndex = searchAlgos.LinearSearch(editList, val); // Returns value's unsorted index
						searchAlgos.PrintValues(); // Will print out closest values if user value not found
					}
					else // Creates, sorts and searches binary tree
					{
						BinarySearchTree bst = new BinarySearchTree();
						bst.CreateBST(editList, true, val, increment, false); // false = ascending order
					}
					break;
				}
				else { Console.WriteLine("\nPlease enter a value between 1 to 3.\n"); }
			}
			return 0; // Flag to loop through menu again

			// Prints out menu options
			void printOpts(string[] opts)
			{
				int n = 1;
				// Prints out each option give
				foreach (string s in opts)
				{
					Console.WriteLine("||{0} - {1}		||", n, s);
					n++;
				}
				Console.WriteLine("||0 - Exit				||");
				Console.Write("==========================================\n>> ");
			}
		}


		/* Get text files and adds their data to lists, assigned to a key with the same name as the file in a dictionary
		*  Gets each non-empty line from txt and adds it to a temporary list
		* List is value, key is formated file name */
		SortedDictionary<string, List<double>> GetData()
		{
			// Stores list
			SortedDictionary<string, List<double>> weatherData = new SortedDictionary<string, List<double>>();
			List<double> tmpList = new List<double>(); // faster than txtLine.Clear() according to https://www.dotnetperls.com/list-clear
													   
			string path = @"Data"; // Path to where txt files are stored

			try
			{
				// Goes through each file in folder and copies contents to list
				foreach (string txtFile in Directory.EnumerateFiles(path, "*.txt"))
				{
					var file = File.ReadLines(txtFile);
					foreach (string line in file)
					{
						if (line.Trim() != "") // Only copy lines that have a value
						{
							try
							{
								tmpList.Add(Convert.ToDouble(line));
							}
							catch (FormatException)
							{
								Console.WriteLine("Value(s) in {0} in incorrect format.", txtFile.Substring(5));
								tmpList = new List<double>(); // Resets list
								break; // Breaks loops to add nextfile to list
							}
						}
					}

					// Adds file name as key and it's cleaned contents in list as value
					if (tmpList.Count != 0) // If list not empty, adds to dict
					{
						weatherData.Add(txtFile.Remove(txtFile.Length - 4).Substring(5), tmpList); // Removes "Data/" and ".txt" for key name
					}
					tmpList = new List<double>(); // Resets list for next use
				}
			}
			catch (DirectoryNotFoundException)
			{
				Console.WriteLine("Please make sure the folder 'Data' is in the same folder as the .exe (~/SSWeatherData), and restart the application."); // Error message
			}

			// Array holding names of lists to merge
			string[] tmpArr = new string[] { "Low_256", "High_256", "Merged_256", "Low_2048", "High_2048", "Merged_2048", "Low_4096", "High_4096", "Merged_4096" };

			// Creates merged lists for low & high _256, _2048 and _4096
			for (int i = 0; i < tmpArr.Length; i += 3)
			{
				tmpList = new List<double>();
				try
				{
					tmpList = mergeLists(weatherData[tmpArr[i]], weatherData[tmpArr[i + 1]]);
					weatherData.Add(tmpArr[i + 2], tmpList);
				}
				catch (KeyNotFoundException)
				{
					Console.WriteLine("List {0} could not be created due to missing lists {1} and {2}", tmpArr[i+2], tmpArr[i], tmpArr[i + 1]);
				}
			}
			// Return empty list, add option in menu only to retry, else print all other options (index 0 vs >1)
			return weatherData;

			// Function that merges 2 given lists
			// Adds every value from list2 to list1, then returns list1
			List<double> mergeLists(List<double> l1, List<double> l2)
			{
				List<double> lTemp = new List<double>();
				for (int i = 0; i < l1.Count; i++)
				{
					lTemp.Add(l1[i]);
					lTemp.Add(l2[i]);
				} 
				return lTemp;
			}
		}
	}


	// Class containing sorting algorithms
	class SortAlgorithms
	{
		public int counter = 0; // Keeps track of number of steps in each program

		// Merge sort function
		public void MergeSort(List<double> list, int mid, int end, bool descending)
		{
			// Checks if element already divided down to single node
			counter += 1;
			if (list.Count > 1)
			{
				mid = (list.Count / 2) - 1;
				end = list.Count - 1; // List length

				// Temp holding lists for seperating left and right sides of lists
				List<double> listLeft = new List<double>();
				List<double> listRight = new List<double>();

				// Copies either side of list for further division till length is 1
				// Copy left side of list to temp list
				for (int i = 0; i <= mid; i++)
				{
					listLeft.Add(list[i]);
					counter += 1;
				}

				// Copy right side of list to a different temp list
				for (int i = mid + 1; i <= end; i++)
				{
					listRight.Add(list[i]);
					counter += 1;
				}

				// seperates lists then merges them
				MergeSort(listLeft, 0, mid, descending);
				MergeSort(listRight, mid + 1, end, descending);
				merge(listLeft, listRight, descending);
				counter += 3;
			}

			// Sorts and inserts values back into original list in order
			void merge(List<double> listLeft, List<double> listRight, bool desOrder)
			{
				int listInd = 0, leftIndex = 0, rightIndex = 0; // main list, left list and right list incremments

				// while both list havent reached the end
				while (leftIndex < listLeft.Count && rightIndex < listRight.Count)
				{
					counter += 2;
					// If desOrder = false: If value in left list less than one in right list, add it to the main list
					// If desOrder = true: If value in right list less than one in left list, add it to the main list
					counter += 2;
					if ((desOrder == false && listLeft[leftIndex] < listRight[rightIndex]) || (desOrder == true && listLeft[leftIndex] > listRight[rightIndex]))
					{
						list[listInd] = listLeft[leftIndex];
						leftIndex++;
						counter += 1;
					}
					else // if not, add right list's value to the main list
					{
						list[listInd] = listRight[rightIndex];
						rightIndex++;
						counter += 1;
					}
					listInd++;
				}

				// Copies any elements leftover from sublists to original list
				// Only one of these will run so will also be in order
				for (int i = leftIndex; i <= listLeft.Count - 1; i++)
				{
					list[listInd] = listLeft[i];
					listInd++;
					counter += 1;
				}

				for (int j = rightIndex; j <= listRight.Count - 1; j++)
				{
					list[listInd] = listRight[j];
					listInd++;
					counter += 1;
				}
			}
		}


		// Quick sort funtion, takes last element as pivot
		// Recurviely runs Quicksort till list down to 1 element
		public void QuickSort(List<double> list, int start, int end, bool desOrder)
		{
			// If there are still elements unsorted in list
			counter += 1;
			if (start < end)
			{
				// seperates lists then swaps around pivot
				int mid = split(list, start, end);

				QuickSort(list, start, mid-1, desOrder);
				QuickSort(list, mid+1, end, desOrder);
				counter += 3;
			}

			// Swaps element smaller than pivot
			int split(List<double> l, int s, int e)
			{
				double temp; // temp placeholder for swapping variables
				int j = s - 1; // index of bigger element

				double pivot = list[e]; // Element to sort around
				counter += 1;

				for (int i = s; i < e; i++)
				{
					// If value is bigger/equal to pivot
					counter += 2;
					if ((list[i] <= pivot && desOrder == false) || (list[i] >= pivot && desOrder == true))
					{
						// Swaps position of 2 items in list
						j++;
						temp = list[j];
						list[j] = list[i];
						list[i] = temp;
						counter += 3;
					}
				}

				// Swaps position of 2 items in list
				temp = list[j+1];
				list[j+1] = list[end];
				list[end] = temp;

				counter += 3;

				return (j + 1); // Returns new midpoint
			}
		}


		// Bubble sort
		double tmp; // Temporay variable for swapping items in list
		public void BubbleSort(List<double> list, bool descending)
		{
			bool sorted = false; // flag - whether list is sorted or not

			// Iteration runs function if list not sorted yet
			while (!sorted) {
				sorted = true;
				// Loops through each item i the list
				for (int i = 0; i < list.Count - 1; i++)
				{
					counter += 2;
					// If next element is less than current, swap them
					if ((descending == false && list[i] > list[i + 1]) || (descending == true && list[i] < list[i + 1]))
					{
						// Swaps the position of the 2 elements in the list
						sorted = false;
						tmp = list[i];
						list[i] = list[i + 1];
						list[i + 1] = tmp;
						counter += 3;
					}
				}
			}
		}

		// Prints out every nth value from list, where n = increment, either in ascending or descending order
		public void PrintList(List<double> list, int increment)
		{
			// 0 = ascending, 1 = descending
			Console.WriteLine("List length: {0}", list.Count);
			// Prints list in ascending order
			for (int i = 0; i <= list.Count-1; i += increment)
			{
				Console.Write("{0}, ", list[i]);
			}
			Console.Write("END\n");
			Console.WriteLine("Number of steps: {0}", counter);
		}
	}


	// Search algorithms
	class SearchAlgorithms
	{
		public Dictionary<int, double> valsDict = new Dictionary<int, double> { };
		bool valFound = false;
		int counter = 0;

		// Linear search algorithm for finding value's index in unsorted list
		public int LinearSearch(List<double> editList, double value)
		{
			for (int i = 0; i < editList.Count; i++)
			{
				counter += 1;
				// If user selects linear search, also searches for closes values
				if (editList[i] == value) { return i; }
			}
			return -1;
		}


		// Binary search algorithm
		public void BinarySearch(List<double> list, int start, int mid, int end, double value)
		{
			// searches for value in list
			// Not finding first or last index
			counter += 2;
			if (start <= end)//(start != mid && mid != end)
			{
				mid = (end + start) / 2; // Set index

				// If value less than mid, search left side
				counter += 1;
				if (list[mid] > value)
				{
					ClosestValues(list, mid, value);
					BinarySearch(list, start, mid, mid - 1, value); // Recursively run till found or not
					counter += 1;
				}
				else if (list[mid] < value) // If value is greater then mid
				{
					counter += 1;
					ClosestValues(list, mid, value);
					BinarySearch(list, mid + 1, mid, list.Count - 1, value); // Recursively run till found or not
					counter += 1;
				}
				else // Value found
				{
					counter += 1; // for above else if
					DuplicateValues(list, mid, value); // Checks for duplicates of value in list
					valFound = true; // Value found flag
				}
			}
			else
			{
				// Throws error if value not found in list and prints error message
				try
				{
					if (valFound == false) { throw new ValueNotFoundException(); }
				}
				catch (ValueNotFoundException) // e
				{

					Console.WriteLine("Value does not exist within selected list");
				}
			}
		}

		// Basic error class if value isn't found within a list
		public class ValueNotFoundException : Exception
		{
			public ValueNotFoundException() : base("Value does not exist within selected list") { }
		}

		// Checks for duplicates of user's values are returns their index
		public void DuplicateValues(List<double> list, int mid, double value)
		{
			int tempMid = mid;

			// If value found print out all it's locations
			while (list[tempMid] == value && tempMid > 0) // Checks for duplicates of values before value in list
			{
				Console.WriteLine("Value {0} found at index {1}", value, tempMid);
				tempMid--;
			}

			while (list[tempMid] == value && tempMid < list.Count) // Checks for duplicates of values after value in list
			{
				Console.WriteLine("Value {0} found at index {1}", value, tempMid);
				tempMid++;
			}
		}

		// Prints user selected value's index
		public void PrintValues()
		{
			if (valFound == false)
			{
				Console.WriteLine("Value not found, next closest values:");
				// Prints out value and it's index
				foreach (KeyValuePair<int, double> ind in valsDict)
				{
					Console.WriteLine("Value: {0}, Index: {1}", ind.Value, ind.Key);
				}
			}
			Console.WriteLine("Number of steps: " + counter);
		}

		// keeps track of closest values to user given value
		public Dictionary<int, double> ClosestValues(List<double> list, int index, double value)
		{
			valsDict = new Dictionary<int, double> { { index, list[index] } }; // Index: value, stores the closest values found, incl. dups
			int i = 1;

			// Gets closest highest value to user given value
			// e.g. user value = 14, returns 14.1
			while (value < list[index - i + 1] && index - i >= 0) // -1 so also adds 1 thats below value
			{
				valsDict.Add(index - i, list[index - i]);
				i++;
			}

			i = 1; //resets i

			// Gets closest lowest value to user given value
			// e.g. user value = 14, returns 13.9
			while (value > list[index + i - 1] && index + i < list.Count) // +1 so also adds 1 thats above value
			{
				valsDict.Add(index + i, list[index + i]);
				i++;
			}
			return valsDict;
		}
	}


	// Binary tree algorithm
	class BinarySearchTree
	{
		// Utiliy function that creates the BST, states whether it's balanced or not, and if given, serarchs for a user given value
		public void CreateBST(List<double> list, bool print, double? val, int increment, bool descending)
		{
			BST bst = new BST();

			// Loop that creates and inserts nodes into tree
			foreach (double d in list)
			{
				bst.InsertNode(bst.root, bst.CreateNode(d));
			}

			// Stores BST's nodes in BST order
			if (descending) { bst.StoreNodesBackwards(bst.root); }
			else { bst.StoreNodes(bst.root); }
				

			Console.WriteLine("Number of steps for creating sorted unbalanced binary search tree: " + bst.counterSort);

			// Checks if tree is balanced, if not, creates balanced tree for optimised searching
			if (bst.IsBalanced(bst.root))
			{
				Console.WriteLine("Tree is balanced");
			}
			else
			{
				Console.WriteLine("Tree is not balanced, creating balanced tree...");

				bst.counterSort = 0;
				bst.root = bst.BalanceTree(bst.sortedTree, 0, bst.sortedTree.Count-1); // Creates a balanced tree
				Console.WriteLine("Tree is balanced: {0}", bst.IsBalanced(bst.root)); // Prints whether tree is now balanced
			}

			// Inorder traversal of tree
			if (print)
			{
				Console.WriteLine("In Order traversal of balanced tree: ");
				bst.InorderTravel(bst.root, increment);
				Console.WriteLine("END");
			}
			Console.WriteLine("Number of steps for creating sorted balanced binary search tree: " + bst.counterSort);

			if (val != null) // If user value given
			{
				// if value not found, return closest values
				bst.FindNode(bst.root, (double)val);
				if (!bst.valueFound)	
				{
					foreach (double d in bst.closestVals)
					{
						Console.WriteLine("Closest values: " + d);
					}
				}
				else
				{
					Console.WriteLine("Value found");
				}
				Console.WriteLine("Number of search steps: " + bst.counterSearch);
			}

		}

		// Node template class, link nodes, store data
		class Node
		{
			public double data;
			public Node left;
			public Node right;

			public Node(double d)
			{
				data = d;
				left = right = null;
			}
		}

		//BST template class
		class BST
		{
			public int counterSort = 0;
			public int counterSearch = 0;
			public Node root;

			public List<Node> sortedTree = new List<Node>(); // unbalanced tree's nodes
			public Dictionary<double, int> dupNodes = new Dictionary<double, int>(); // Records duplicate nodes
			public List<double> closestVals = new List<double>(); // Used in ClosestValues()
			public bool valueFound;

			// Declares tree entry point
			public BST() { root = null; }

			// Adds new node to tree
			public Node CreateNode(double d)
			{
				// If node empty, assign new value
				counterSort += 1;
				if (root == null)
				{
					root = new Node(d);
					counterSort += 1;
				}
				return new Node(d);
			}

			// Checks if binary tree is balanced (difference between heights is no more than 1 level)
			// recursivly adds 1 to each height
			public bool IsBalanced(Node node)
			{
				int leftHeight;
				int rightHeight;

				// If tree empty or reached end of tree
				if (node == null) { return true; }

				// Checks left and right subtree heights
				leftHeight = height(node.left);
				rightHeight = height(node.right);

				if (Math.Abs(leftHeight - rightHeight) <= 1 && IsBalanced(node.left) && IsBalanced(node.right))
				{
					return true; // tree is balanced
				}
				return false; // tree is unbalanced

				// gets tree height?
				int height(Node n)
				{
					if (n == null) { return 0; }
					return 1 + Math.Max(height(n.left), height(n.right));
				}
			}


			// Inserts node into tree
			public void InsertNode(Node root, Node newNode)
			{
				/* Selects correct node to link new node to
				 * left if new node is less than parent node, right if it's more than.
				 * If node is already occupied, recursively searches for next available node
				 */
				counterSort += 1;
				if (root != null)
				{
					counterSort += 1;
					if (newNode.data > root.data)
					{
						counterSort += 1;
						if (root.right != null)
						{
							InsertNode(root.right, newNode); // Recursively looks for next free node
							counterSort += 1;
						}
						else
						{
							root.right = newNode; // Sets node to right of (more than) parent node
							counterSort += 1;
						} 
					}
					else if (newNode.data < root.data)
					{
						counterSort += 1;
						if (root.left != null)
						{
							InsertNode(root.left, newNode); // Recursively looks for next free node
							counterSort += 2;
						}
						else
						{
							root.left = newNode; // Sets node to left of (less than) parent node
							counterSort += 2;
						} 
					}
					else // New node values are the same, add value in dict
					{
						// Duplicate node value found, interate value in dict and break
						counterSort += 2;
						if (!dupNodes.ContainsKey(newNode.data)) // If dups not in dict yet, create and add it
						{
							dupNodes.Add(newNode.data, 2); // Default 2 as 2 sets of value found
							counterSort += 1;
						}
						else
						{
							++dupNodes[newNode.data];
							counterSort += 1;
						}
						return;
					}
				}
			}

			// Returns value
			// If correct node not found, recursively look for correct node until found or searched entire tree
			public void FindNode(Node node, double toFind)
			{
				counterSearch += 1;
				if (node != null)
				{
					counterSearch += 1;
					if (toFind != node.data)
					{
						counterSearch += 1;
						// If data at node is greater than value, move to left child node, else move down to right child node
						if (toFind < node.data)
						{
							ClosestValues(node, toFind); // Look for closest value to user give value
							counterSearch += 1;
							FindNode(node.left, toFind); // Recursively search for user give value
						}
						else
						{
							ClosestValues(node, toFind); // Look for closest value to user give value
							counterSearch += 1;
							FindNode(node.right, toFind); // Recursively search for user give value
						}

						// If value range outside of nodes, selects closest value to user given value and removes extra value
						if ((node.left == null || node.right == null) && closestVals.Count == 2) // Iff no more nodes to check
						{
							if (!(closestVals[0] < toFind && closestVals[1] > toFind) && !(closestVals[0] > toFind && closestVals[1] < toFind))
							{
								if (closestVals[0] < toFind && closestVals[1] < toFind) // Both less than
								{
									closestVals.RemoveAt(1); // Keeps value closer to user given value
								}
								else // Both more than
								{
									closestVals.RemoveAt(0); // Keeps value closer to user given value
								}
							}
						}
					}
					else
					{
						valueFound = true; // sets to true if found
						return;
					}
				}
			}


			// keeps track of closest values to user given value
			// Finds closest/same values to user give value in BST
			public void ClosestValues(Node n, double value)
			{
				double checkHigh; double checkLow; double checkValue;

				if (n == null) { return; }

				// If nothing in list, add data for comparisions
				if (closestVals.Count == 0) { closestVals.Add(n.data); closestVals.Add(n.data); }
				else
				{
					// Ensures dictionary has found close values above and below given value
					if (closestVals[0] != n.data && closestVals[1] != n.data) // Ensures same value isnt being added twice
					{
						// Checks if current item in list or new node's data is closer to the value
						checkLow = closestVals[0] - value;
						checkHigh = closestVals[1] - value;
						checkValue = n.data - value;

						// If data less than and closer to value, replace old data in list
						// Checks if new data is closer to value then currently stored
						if (n.data < value && Math.Abs(checkValue) < Math.Abs(checkLow))
						{
							closestVals[0] = n.data; 
						}
						if (n.data > value && Math.Abs(checkValue) < Math.Abs(checkLow))
						{
							closestVals[1] = n.data; 
						}
					}
				}
			}

			// Balancing unbalanced BST nlog(n), more efficient to create new one
			// Creates new balanced tree from sorted list log(n) search
			public Node BalanceTree(List<Node> nodes, int start, int end) // Node r, int start, int mid, int end)
			{
				// Gets sorted list's middle value to use as root, the middle value between 2 constraints
				// Sorting time of log(n)

				// Gets midpoint of "new" list
				if (start > end)
				{
					counterSort += 1;
					return null;
				}

				int midPoint = (end + start) / 2;
				Node node = nodes[midPoint];

				counterSort += 1;

				// Links subtrees to top root
				node.left =  BalanceTree(nodes, start, midPoint - 1);
				node.right = BalanceTree(nodes, midPoint + 1, end);

				counterSort += 2;

				return node;
			}

			int i = -1;
			// Prints sorted tree, if ordered, proves validity of BST
			public void InorderTravelBackwards(Node node, int increment)
			{
				string dupVal = "";
				if (node != null)
				{
					// Check if value is a duplicate
					if (dupNodes.ContainsKey(node.data)) { dupVal = "(" + dupNodes[node.data] + ")"; }
					InorderTravelBackwards(node.right, increment);

					i++;
					// Print every nth node in tree
					if (i % increment == 0) { Console.Write("{0}{1} ", node.data, dupVal); }
					InorderTravelBackwards(node.left, increment);
				}
			}

			// Prints sorted tree, if ordered, proves validity of BST
			public void InorderTravel(Node node, int increment)
			{
				string dupVal = "";
				if (node != null)
				{
					// Check if value is a duplicate
					if (dupNodes.ContainsKey(node.data)) { dupVal = "(" + dupNodes[node.data] + ")"; }
					InorderTravel(node.left, increment);
					i++;
					// Print every nth node in tree
					if (i % increment == 0) { Console.Write("{0}{1} ", node.data, dupVal); }
					InorderTravel(node.right, increment);
				}
			}

			// Goes through tree stroing nodes in sorted order for bst
			// Also stores values in printing order
			public void StoreNodes(Node n)
			{
				// Stores nodes inorder for creation of balanced tree
				if (n == null) { return; } // doesn't store nodes with null value
				StoreNodes(n.left);
				sortedTree.Add(n);
				StoreNodes(n.right);
			}

			public void StoreNodesBackwards(Node n)
			{
				// Stores nodes inorder for creation of balanced tree
				if (n == null) { return; } // doesn't store nodes with null value
				StoreNodesBackwards(n.left);
				sortedTree.Add(n);
				StoreNodesBackwards(n.right);
			}
		}
	}
}